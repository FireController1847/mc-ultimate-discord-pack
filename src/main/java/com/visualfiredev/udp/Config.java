package com.visualfiredev.udp;

import java.util.Arrays;

import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.common.config.Property;

public class Config {

	// Categories
	public static String rp = "Rich Presence";

	// Properties
	public static Property rp_enabled;
	public static Property rp_application_id;
	public static Property rp_modpack_name;
	public static Property rp_modpack_id;

	// The Actual Config
	public static Configuration config;

	public static void setup() {
		// Rich Presence
		rp_enabled = config.get(rp, "Enabled", true);
		rp_enabled.setComment("Enables or disables Discord Rich Presence. [Default: true]");
		rp_application_id = config.get(rp, "Application ID", "453398945406910474");
		rp_application_id.setComment(
				"The ID of the application that will be used to get the images and name of the game. [Default: '453398945406910474']");
		rp_modpack_name = config.get(rp, "Modpack Name", "Vanilla");
		rp_modpack_name.setComment(
				"The name that will display next to your current status. (Ex. Main Menu | Vanilla) [Default: 'Vanilla']");
		rp_modpack_id = config.get(rp, "Modpack ID", "vanilla");
		rp_modpack_id.setComment(
				"The key that will be used to set the small circle in the bottom right corner of your presence. [Default: 'vanilla']");
		// Set Order
		config.setCategoryPropertyOrder(rp, Arrays.asList("Enabled", "Application ID", "Modpack Name", "Modpack ID"));
		// Save
		if (config.hasChanged()) {
			config.save();
		}
	}
}
