package com.visualfiredev.udp;

public class Reference {

	public static final String INITIALS = "UDP";
	public static final String MOD_ID = "ultimatediscordpack";
	public static final String NAME = "Ultimate Discord Pack";
	public static final String VERSION = "0.1.0";
	public static final String MCVERSIONS = "[1.12,1.13)";
	public static final String CLIENT_PROXY = "com.visualfiredev.udp.proxy.ClientProxy";
	public static final String SERVER_PROXY = "com.visualfiredev.udp.proxy.ServerProxy";

}
