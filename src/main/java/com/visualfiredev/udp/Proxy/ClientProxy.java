package com.visualfiredev.udp.proxy;

import com.visualfiredev.udp.Config;
import com.visualfiredev.udp.UltimateDiscordPack;
import com.visualfiredev.udp.richpresence.RPEventHandler;
import com.visualfiredev.udp.richpresence.RPHandler;

import net.minecraftforge.common.MinecraftForge;

public class ClientProxy implements IProxy {

	@Override
	public void preInit() {
		if (Config.rp_enabled.getBoolean()) {
			UltimateDiscordPack.rphandler = new RPHandler();
			UltimateDiscordPack.rphandler.client
					.setPresence(UltimateDiscordPack.rphandler.dimensions.get(1921).resetTimestamp());
		}
	}

	@Override
	public void init() {
		if (Config.rp_enabled.getBoolean())
			MinecraftForge.EVENT_BUS.register(new RPEventHandler());
	}

	@Override
	public void postInit() {
		// TODO Auto-generated method stub

	}

}
