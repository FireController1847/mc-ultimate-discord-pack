package com.visualfiredev.udp.richpresence;

import java.util.HashMap;
import java.util.Map;

import com.visualfiredev.udp.Config;
import com.visualfiredev.udp.UltimateDiscordPack;

public class RPHandler {

	public static final RPClient client = new RPClient();

	public static final Map<Integer, RichPresence> dimensions = new HashMap<Integer, RichPresence>();
	public static final Map<String, String> gameTypes = new HashMap<String, String>();

	public RPHandler() {
		fillMaps();
		client.init();
	}

	public static void setToMainMenu() {
		UltimateDiscordPack.rphandler.client.setPresence(dimensions.get(1922).resetTimestamp());
	}

	/*
	 * There are three types... "singleplayer" = "Playing Singleplayer" "lan" =
	 * "Playing Multiplayer (LAN)" "multiplayer" = "Playing Multiplayer"
	 */
	public static void setToDimension(int id, String type, boolean resetTime, HashMap<String, String> overrides) {
		RichPresence presence = dimensions.get(id);
		if (resetTime) {
			for (Map.Entry<Integer, RichPresence> entry : dimensions.entrySet()) {
				entry.getValue().resetTimestamp();
			}
		}
		if (overrides != null) {
			for (Map.Entry<String, String> entry : overrides.entrySet()) {
				UltimateDiscordPack.logger.info("OVERRIDES ENTRY: " + entry.getKey() + " -- " + entry.getValue());
			}
		}
		UltimateDiscordPack.rphandler.client.setPresence(presence.setState(gameTypes.get(type)));
	}

	private static void fillMaps() {
		//// Dimensions
		String mpn = Config.rp_modpack_name.getString();
		// UDP
		dimensions.put(1921, new RichPresence().setDetails("Launching | " + mpn).setLargeImageKey("loading"));
		dimensions.put(1922, new RichPresence().setDetails("Main Menu | " + mpn).setLargeImageKey("crafting_large"));
		// Vanilla
		dimensions.put(-1, new RichPresence().setDetails("In The Nether | " + mpn).setLargeImageKey("nether"));
		dimensions.put(0, new RichPresence().setDetails("In The Overworld | " + mpn).setLargeImageKey("overworld"));
		dimensions.put(1, new RichPresence().setDetails("In The End | " + mpn).setLargeImageKey("the_end"));

		//// Game Types
		gameTypes.put("singleplayer", "Playing Singleplayer");
		gameTypes.put("lan", "Playing Multiplayer (LAN)");
		gameTypes.put("multiplayer", "Playing Multiplayer");
	}

}
