package com.visualfiredev.udp.richpresence;

import com.visualfiredev.udp.Config;

import club.minnced.discord.rpc.DiscordRichPresence;
import net.minecraftforge.fml.common.Loader;

public class RichPresence extends DiscordRichPresence {

	public RichPresence() {
		this.startTimestamp = System.currentTimeMillis() / 1000;
		this.largeImageText = "Minecraft " + Loader.MC_VERSION;
		this.smallImageText = Config.rp_modpack_name.getString();
		this.smallImageKey = Config.rp_modpack_id.getString();
	}

	public RichPresence resetTimestamp() {
		this.startTimestamp = System.currentTimeMillis() / 1000;
		return this;
	}

	public RichPresence setState(String state) {
		this.state = state;
		return this;
	}

	public RichPresence setDetails(String details) {
		this.details = details;
		return this;
	}

	public RichPresence setStartTimestamp(long startTimestamp) {
		this.startTimestamp = startTimestamp;
		return this;
	}

	public RichPresence setEndTimestamp(long endTimestamp) {
		this.endTimestamp = endTimestamp;
		return this;
	}

	public RichPresence setLargeImageKey(String largeImageKey) {
		this.largeImageKey = largeImageKey;
		return this;
	}

	public RichPresence setLargeImageText(String largeImageText) {
		this.largeImageText = largeImageText;
		return this;
	}

	public RichPresence setSmallImageKey(String smallImageKey) {
		this.smallImageKey = smallImageKey;
		return this;
	}

	public RichPresence setSmallImageText(String smallImageText) {
		this.smallImageText = smallImageText;
		return this;
	}

	public RichPresence setPartyId(String partyId) {
		this.partyId = partyId;
		return this;
	}

	public RichPresence setPartySize(int partySize) {
		this.partySize = partySize;
		return this;
	}

	public RichPresence setPartyMax(int partyMax) {
		this.partyMax = partyMax;
		return this;
	}

	public RichPresence setMatchSecret(String matchSecret) {
		this.matchSecret = matchSecret;
		return this;
	}

	public RichPresence setJoinSecret(String joinSecret) {
		this.joinSecret = joinSecret;
		return this;
	}

	public RichPresence setInstance(byte instance) {
		this.instance = instance;
		return this;
	}

}
