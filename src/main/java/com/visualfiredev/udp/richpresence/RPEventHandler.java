package com.visualfiredev.udp.richpresence;

import java.util.HashMap;

import com.visualfiredev.udp.UltimateDiscordPack;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiMainMenu;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.multiplayer.ServerData;
import net.minecraft.server.integrated.IntegratedServer;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent.ClientTickEvent;

public class RPEventHandler {

	// Rich Presence Settings
	private static ServerData prevServerData = null;
	private static Integer prevDimension = null;
	private static GuiScreen prevGui = null;
	private static String prevGameType = null;
	private static Boolean isMainMenu = false;
	private static Boolean initialJoin = true;

	// Loop Settings
	private static int secondsPerUpdate = 2;
	private static int ticksPerSecond = 40;
	private static int ticks = 0;

	@SubscribeEvent
	public void onClientTick(ClientTickEvent event) {
		ticks++;
		if (ticks >= ticksPerSecond * secondsPerUpdate) {
			UltimateDiscordPack.logger.info("CHECK FOR PRESENCE UPDATE");
			Minecraft minecraft = Minecraft.getMinecraft();
			ServerData server = minecraft.getCurrentServerData();
			IntegratedServer iserver = minecraft.getIntegratedServer();
			// MainMenu
			if (minecraft.currentScreen == null || !(minecraft.currentScreen instanceof GuiMainMenu)) {
				isMainMenu = false;
			}
			if (minecraft.currentScreen instanceof GuiMainMenu && !isMainMenu) {
				UltimateDiscordPack.logger.info("SET TO MAIN MENU");
				// Set to Main Menu!
				RPHandler.setToMainMenu();
				isMainMenu = true;
				initialJoin = true;
				prevGameType = null;
			}
			// Singleplayer
			if ((server == null && (iserver == null || !iserver.getPublic()
					|| iserver.getPlayerList().getCurrentPlayerCount() <= 1)) && minecraft.player != null) {
				UltimateDiscordPack.logger.info("SET TO SINGLEPLAYER");
				checkDimensionThenSet(minecraft, "singleplayer", initialJoin);
				prevGameType = "singleplayer";
				initialJoin = false;
			}
			// Servers
			if ((server != null
					|| (iserver != null && iserver.getPublic() && iserver.getPlayerList().getCurrentPlayerCount() > 1))
					&& minecraft.player != null) {
				if (server == null) {
					// Local Server
					UltimateDiscordPack.logger.info("SET TO LAN");
					checkDimensionThenSet(minecraft, "lan", initialJoin);
					prevGameType = "lan";
					initialJoin = false;
				} else {
					// Public Server
					UltimateDiscordPack.logger.info("SET TO MULTIPLAYER");
					checkDimensionThenSet(minecraft, "multiplayer", initialJoin);
					prevGameType = "multiplayer";
					initialJoin = false;
				}
			}

			prevGui = minecraft.currentScreen;
			if (minecraft.player != null)
				prevDimension = minecraft.player.dimension;
			else
				prevDimension = null;

			ticks = 0;
		}
	}

	public static void checkDimensionThenSet(Minecraft minecraft, String type, boolean resetTime,
			HashMap<String, String> overrides) {
		if (prevDimension == null || prevDimension != minecraft.player.dimension || prevGameType == null
				|| prevGameType != type) {
			UltimateDiscordPack.logger.info("DIMENSION CHANGE, GO!");
			RPHandler.setToDimension(minecraft.player.dimension, type, resetTime, overrides);
		}
	}

	public static void checkDimensionThenSet(Minecraft minecraft, String type, boolean resetTime) {
		checkDimensionThenSet(minecraft, type, resetTime, null);
	}

}
