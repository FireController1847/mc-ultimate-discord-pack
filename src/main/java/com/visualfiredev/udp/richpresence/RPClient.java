package com.visualfiredev.udp.richpresence;

import com.visualfiredev.udp.Config;
import com.visualfiredev.udp.UltimateDiscordPack;

import club.minnced.discord.rpc.DiscordEventHandlers;
import club.minnced.discord.rpc.DiscordRPC;
import club.minnced.discord.rpc.DiscordRichPresence;

public class RPClient {

	public static void init() {
		DiscordEventHandlers handlers = new DiscordEventHandlers();
		UltimateDiscordPack.logger.info("RPC Client Connecting...");
		DiscordRPC.INSTANCE.Discord_Initialize(Config.rp_application_id.getString(), handlers, true, null);
		initThreads();
	}

	private static void initThreads() {
		// Callbacks
		new Thread(() -> {
			while (!Thread.currentThread().isInterrupted()) {
				DiscordRPC.INSTANCE.Discord_RunCallbacks();
				try {
					Thread.sleep(2000);
				} catch (InterruptedException ignored) {
				}
			}
		}, "RPC-Callback-Handler").start();
		// Shutdown
		Runtime.getRuntime().addShutdownHook(new Thread() {
			@Override
			public void run() {
				DiscordRPC.INSTANCE.Discord_Shutdown();
				UltimateDiscordPack.logger.info("RPC Client Disconnected");
			}
		});
	}

	public void setPresence(DiscordRichPresence presence) {
		DiscordRPC.INSTANCE.Discord_UpdatePresence(presence);
	}

}
