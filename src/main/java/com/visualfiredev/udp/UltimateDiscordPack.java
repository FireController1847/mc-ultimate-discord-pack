package com.visualfiredev.udp;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.visualfiredev.udp.proxy.IProxy;
import com.visualfiredev.udp.richpresence.RPHandler;

import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;

@Mod(modid = Reference.MOD_ID, name = Reference.NAME, version = Reference.VERSION, acceptedMinecraftVersions = Reference.MCVERSIONS)
public class UltimateDiscordPack {

	public static final Logger logger = LogManager.getLogger(Reference.INITIALS);
	public static RPHandler rphandler;

	@SidedProxy(clientSide = Reference.CLIENT_PROXY, serverSide = Reference.SERVER_PROXY)
	public static IProxy proxy;

	@EventHandler
	public void preInit(FMLPreInitializationEvent event) {
		logger.info("Pre Initialization");
		Config.config = new Configuration(event.getSuggestedConfigurationFile());
		Config.config.load();
		Config.setup();
		proxy.preInit();
	}

	@EventHandler
	public void init(FMLInitializationEvent event) {
		logger.info("Initialization");
		proxy.init();
	}

	@EventHandler
	public void postInit(FMLPostInitializationEvent event) {
		logger.info("Post Initialization");
		proxy.postInit();
	}

}
